***CardBook*** – *a new Thunderbird address book based on the CardDAV and vCard standards*

--------

### Features

* **Autocompletion** in mail address fields and [Lightning](https://addons.thunderbird.net/addon/lightning/) calendar fields
* Manage contacts from messages
* Restrict address book use by mail account (for email autocompletion, email collection and contact registration)
* Easy **[CardDAV](https://en.wikipedia.org/wiki/CardDAV) synchronization**
* Access to **all [vCard](https://en.wikipedia.org/wiki/VCard) data**
* Supports [vCard 3.0](https://en.wikipedia.org/wiki/VCard#vCard_3.0) and [vCard 4.0](https://en.wikipedia.org/wiki/VCard#vCard_4.0) (default)
* **Customizable** data fields
* Unlimited number of custom fields
* Address panel
* Show address on map
* Phone number validation
* Unlimited phone, addresses, etc per contact
* Connect directly to chat programs (Google, [Jabber](https://www.jabber.org/), [Skype](https://www.skype.com/), [QQ](http://www.imqq.com/)) to chat using [IMPP](https://en.wikipedia.org/wiki/Instant_Messaging_and_Presence_Protocol)
* Connect directly to softphone ([Linphone](https://www.linphone.org/), [Ring](https://ring.cx/)) to make phone calls using [SIP](https://en.wikipedia.org/wiki/Session_Initiation_Protocol)
* [Digest access authentication](https://en.wikipedia.org/wiki/Digest_access_authentication)
* [Drag and drop](https://en.wikipedia.org/wiki/Drag_and_drop) support
* [Saved search](https://en.wikipedia.org/wiki/Saved_search)
* Filter
* Merge contacts modified both locally and on the server
* Find and merge duplicate contacts
* Find all emails related to a contact
* Find all calendar entries related to a contact
* Find ignoring diacritical marks
* Share contacts by email
* Collect outgoing emails
* Contacts stored in [IndexedDB](https://en.wikipedia.org/wiki/Indexed_Database_API)
* Customizable printouts
* Import Thunderbird standard address books
* [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) file export and import
* [VCF](https://en.wikipedia.org/wiki/VCard) file export and import
* Functional [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface)
* Classic and vertical layout
* Anniversary manager
* Export birthdays to [Lightning](https://addons.thunderbird.net/addon/lightning/) add-on
* Task manager
* Works with 
[1&1 MailXchange](https://www.1and1.com/webmail), 
[Baïkal Server](http://sabre.io/baikal/), 
[Cozy](https://cozy.io/), 
[DAVdroid](https://www.davdroid.com/), 
[DAViCal](https://www.davical.org/), 
[Deepen Dhulla OpenSync App for Android](https://deependhulla.com/android-apps/opensync-app), 
[Fastmail](https://www.fastmail.com/), 
[Framagenda](https://framagenda.org/), 
[GMX](https://www.gmx.com/), 
[Google](https://www.google.com/), 
[iCloud](https://www.icloud.com/), 
[mailbox.org](https://mailbox.org/), 
[Memotoo](https://www.memotoo.com/), 
[MyPhoneExplorer](https://www.fjsoft.at/), 
[Nextcloud](https://nextcloud.com/), 
[ownCloud](https://owncloud.org/), 
[Posteo](https://posteo.de/), 
[Radicale](https://radicale.org/), 
[Synology](https://www.synology.com/), 
[Yahoo!](https://login.yahoo.com), 
[Yandex](https://yandex.com/), 
[Zimbra](https://www.zimbra.com/), 
macOS Contacts Server, 
and more
* Available in many [languages](https://gitlab.com/CardBook/CardBook#translators)
* Supported operating systems 
[Apple macOS](https://en.wikipedia.org/wiki/MacOS), 
[BSD](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution), 
[Linux](https://en.wikipedia.org/wiki/Linux), 
[Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)
* Standalone or fully integrated within Thunderbird
* Fully integrated in the main Thunderbird window (yellow
stars, possibility to edit|remove|add standard and CardBook contacts)
* [Free/Libre Open Source Software (FLOSS)](https://www.gnu.org/philosophy/floss-and-foss.en.html)
* [User Forum](https://cardbook.6660.eu/)
* [@CardBook](https://twitter.com/CardBookAddon) twitter
* *And many more…*


### Installation

You can get CardBook from the [official Thunderbird add-on page](https://addons.thunderbird.net/addon/cardbook/)!

Once CardBook is installed, you can also run CardBook as a standalone program:<br>
Windows `thunderbird.exe -cardbook`<br>
OSX `thunderbird -cardbook`<br>
Linux `thunderbird -cardbook`

### Compatible and complementary add-ons

* [Lightning](https://addons.thunderbird.net/addon/lightning/)
* [Mail Merge](https://addons.thunderbird.net/addon/mail-merge/)
* [Mail Redirect](https://addons.thunderbird.net/addon/mailredirect/)
* [TbSync (Provider for Exchange ActiveSync)](https://addons.thunderbird.net/addon/tbsync/)


### Issues

If you encounter any problems with CardBook please have a look at our [GitLab issue tracker](https://gitlab.com/CardBook/CardBook/issues) and [our forum](https://cardbook.6660.eu/).
If your problem is not listed there, you can do us a great favor by creating a new issue. But even if there is already an issue discussing the topic, you could help us by providing additional information in a comment.

When you are creating an issue, please give the following information:
* a detailed description of the problem
* the situation the problem occurs in and how other people will be able to recreate it, i.e. the steps to reproduce the problem
* your Thunderbird version (you can find this in the main menu at `Help → About Thunderbird`)
* your CardBook version (you can find this in the CardBook Preferences window [title bar](https://en.wikipedia.org/wiki/Window_decoration#Title_bar) `CardBook → Preferences`)
* your operating system and version
* if possible: the **relevant** output of the error console (found at `Tools → Developer Tools → Error Console` / `Ctrl-Shift-J`)


### [Manual](https://gitlab.com/CardBook/CardBook/wikis/home)
CardBook provides an integrated contact management solution with a fully functional and logical [UX](https://en.wikipedia.org/wiki/User_experience) and [UI](https://en.wikipedia.org/wiki/User_interface).

CardBook has been designed to be an efficient and effective tool that does not need a manual to become productive. However, the [CardBook Manual](https://gitlab.com/CardBook/CardBook/wikis/home) may enable the discovery of the features, benefits and workflows of this tool.

You are encouraged to contribute to the [CardBook Manual](https://gitlab.com/CardBook/CardBook/wikis/home) by:
* writing a page about a [feature](https://gitlab.com/CardBook/CardBook#features)
* something else


### Roadmap

* Place phone calls directly from CardBook’s address book, eg. [TBDialOut](https://addons.thunderbird.net/addon/tbdialout/)
* Displays the contact photo/image/picture of the sender/recipient while reading or composing a message, eg. [Display Contact Photo](https://addons.thunderbird.net/addon/display-contact-photo/)
* Better integration with the standard Thunderbird address book
* About the future of Thunderbird, whatever would be the path to the new Thunderbird, I’m [[Philippe Vigneau](https://mail.mozilla.org/pipermail/tb-planning/2017-April/005378.html)] ready and open to work to make CardBook the new Thunderbird addressbook


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations (i.e. translations) at [locale](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale) or by emailing (in English or French) [cardbook.thunderbird@gmail.com](mailto:cardbook.thunderbird@gmail.com)
* creating [issues](https://gitlab.com/CardBook/CardBook/issues) about problems
* creating [issues](https://gitlab.com/CardBook/CardBook/issues) about possible improvements
* helping people who have problems or questions
* improving the documentation
* working on the code ([Mozilla Coding Style Guide](https://developer.mozilla.org/docs/Mozilla_Coding_Style_Guide))
* emailing [cardbook.thunderbird@gmail.com](mailto:cardbook.thunderbird@gmail.com) news and media article links about CardBook which can be shared [@CardBook](https://twitter.com/CardBookAddon)
* spreading the word about this great add-on
* improving the [CardBook Manual](https://gitlab.com/CardBook/CardBook/wikis/home)
* adding or improving the localizations (i.e. translations) of the [CardBook Manual](https://gitlab.com/CardBook/CardBook/wikis/home)


### Coders
* Philippe Vigneau (author and maintainer)
* [Alexander Bergmann](https://addons.thunderbird.net/user/tempuser/)
* [John Bieling](https://github.com/jobisoft)
* Sisim Biva
* [Günter Gersdorf](https://addons.thunderbird.net/user/guenter-gersdorf/)
* [R. Kent James](https://github.com/rkent)
* Axel Liedtke
* Timothe Litt
* Christoph Mair
* Günther Palfinger
* Boris Prüssmann
* Michael Roland
* Lukáš Tyrychtr
* Marco Zehe

### Translators
* Lukáš Tyrychtr ([cs](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/cs)) [Stáhnout](https://addons.thunderbird.net/cs/addon/cardbook/)
* Johnny Nielsen ([da](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/da)) [Hent nu](https://addons.thunderbird.net/da/addon/cardbook/)
* Alexander Bergmann ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Markus Mauerer ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Oliver Schuppe ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* SusiTux ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Γιώτα ([el](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/el)) [Λήψη τώρα](https://addons.thunderbird.net/el/addon/cardbook/)
* Μαρία ([el](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/el)) [Λήψη τώρα](https://addons.thunderbird.net/el/addon/cardbook/)
* Timothe Litt ([en-US](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/en-US)) [Download Now](https://addons.thunderbird.net/en-US/addon/cardbook/)
* Óvári ([en-US](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/en-US)) [Download Now](https://addons.thunderbird.net/en-US/addon/cardbook/)
* Sylvain Lesage ([es-ES](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/es-ES)) [Descargar ahora](https://addons.thunderbird.net/es-ES/addon/cardbook/)
* Philippe Vigneau ([fr](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/fr)) [Télécharger maintenant](https://addons.thunderbird.net/fr/addon/cardbook/)
* Meri ([hr](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/hr)) [Preuzeti sada](https://addons.thunderbird.net/hr/thunderbird/addon/cardbook/)
* Óvári ([hu](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/hu)) [Letöltés most](https://addons.thunderbird.net/hu/addon/cardbook/)
* Zuraida ([id](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/id)) [Unduh Sekarang](https://addons.thunderbird.net/id/addon/cardbook/)
* Stefano Bloisi ([it](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/it)) [Scarica ora](https://addons.thunderbird.net/it/addon/cardbook/)
* Agnese Morettini ([it](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/it)) [Scarica ora](https://addons.thunderbird.net/it/addon/cardbook/)
* いつき ([ja](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ja)) [今すぐダウンロード](https://addons.thunderbird.net/ja/addon/cardbook/)
* 千秋 ([ja](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ja)) [今すぐダウンロード](https://addons.thunderbird.net/ja/addon/cardbook/)
* 공성은 ([ko](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ko)) [다운로드](https://addons.thunderbird.net/ko/addon/cardbook/)
* Gintautas ([lt](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/lt)) [Parsiųsti dabar](https://addons.thunderbird.net/lt/thunderbird/addon/cardbook/)
* Han Knols ([nl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/nl)) [Nu downloaden](https://addons.thunderbird.net/nl/addon/cardbook/)
* Adam Gorzkiewicz ([pl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pl)) [Pobierz](https://addons.thunderbird.net/pl/addon/cardbook/)
* Dominik Wnęk ([pl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pl)) [Pobierz](https://addons.thunderbird.net/pl/addon/cardbook/)
* [André Bação](https://github.com/abacao) ([pt-PT](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pt-PT)) [Transferir agora](https://addons.thunderbird.net/pt-PT/addon/cardbook/)
* Lucian Burca ([ro](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ro)) [Descarcă acum](https://addons.thunderbird.net/ro/addon/cardbook/)
* Florin Craciunica ([ro](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ro)) [Descarcă acum](https://addons.thunderbird.net/ro/addon/cardbook/)
* Alexander Yavorsky ([ru](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ru)) [Загрузить сейчас](https://addons.thunderbird.net/ru/addon/cardbook/)
* Peter Klofutar ([sl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sl)) [Prenesi zdaj](https://addons.thunderbird.net/sl/addon/cardbook/)
* Anders ([sv-SE](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sv-SE)) [Hämta nu](https://addons.thunderbird.net/sv-SE/addon/cardbook/)
* Kire Dyfvelsten ([sv-SE](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sv-SE)) [Hämta nu](https://addons.thunderbird.net/sv-SE/addon/cardbook/)
* Валентина ([uk](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/uk)) [Завантажити зараз](https://addons.thunderbird.net/uk/addon/cardbook/)
* Христина ([uk](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/uk)) [Завантажити зараз](https://addons.thunderbird.net/uk/addon/cardbook/)
* Loan ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* Nguyễn ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* Thanh ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* [Transvision](https://transvision.mozfr.org/)
* [KDE Localization](https://l10n.kde.org/dictionary/search-translations.php)


#### Trailblazer
* [Thomas McWork](https://github.com/thomas-mc-work): midwife for the public git repository

### License

[Mozilla Public License version 2.0](https://gitlab.com/CardBook/CardBook/blob/master/LICENSE.txt)
