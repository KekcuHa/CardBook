<!ENTITY generalTabLabel "General">
<!ENTITY CardBookTabLabel "Filă CardBook">
<!ENTITY typesTabLabel "Tipuri">
<!ENTITY IMPPsTabLabel "Comunicații">
<!ENTITY customFieldsTabLabel "Câmpuri personalizate">
<!ENTITY listsTabLabel "Liste">
<!ENTITY syncTabLabel "Sincronizare">
<!ENTITY emailTabLabel "Email">
<!ENTITY birthdaylistTabLabel "Aniversare">
<!ENTITY synclistTabLabel "Memento aniversare">

<!ENTITY integrationGroupboxLabel "Integrare în Thunderbird">
<!ENTITY autocompletionLabel "Completare automată a adreselor de email">
<!ENTITY autocompletionAccesskey "o">
<!ENTITY autocompletionTooltip "Extinde completarea automată standard a contactelor din agendele Thunderbird cu o completare automată a contactelor din agendele CardBook.">
<!ENTITY autocompleteSortByPopularityLabel "Sortează adresele de email după popularitate">
<!ENTITY autocompleteSortByPopularityAccesskey "p">
<!ENTITY autocompleteProposeConcatEmailsLabel "Sugerează email-uri concatenate pentru fiecare contact">
<!ENTITY autocompleteProposeConcatEmailsAccesskey "m">
<!ENTITY autocompleteShowAddressbookLabel "Afișează numele agendei">
<!ENTITY autocompleteShowAddressbookAccesskey "n">
<!ENTITY autocompleteWithColorLabel "Utilizează culoarea agendei">
<!ENTITY autocompleteWithColorAccesskey "c">
<!ENTITY autocompleteRestrictSearchLabel "Restricționează câmpurile de căutare">
<!ENTITY autocompleteRestrictSearchAccesskey "R">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonLabel "Editează">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonAccesskey "E">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonLabel "Resetează">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonAccesskey "z">
<!ENTITY useColorLabel "Utilizează culoarea agendei pentru:">
<!ENTITY useColorBackgroundLabel "Fundal">
<!ENTITY useColorBackgroundAccesskey "f">
<!ENTITY useColorTextLabel "Text">
<!ENTITY useColorTextAccesskey "t">
<!ENTITY exclusiveLabel "Sursă unică pentru contacte">
<!ENTITY exclusiveAccesskey "u">
<!ENTITY exclusiveTooltip "Dacă este dezactivată, această opțiune permite preluarea contactelor în agenda standard Thunderbird pentru stelele galbene, pentru afișarea numelui afisat al email-ului și pentru completarea automată a adresei de email.">
<!ENTITY accountsRestrictionsLabel "Restricții cont de email">
<!ENTITY accountsRestrictionsEnabledLabel "Activat">
<!ENTITY accountsRestrictionsMailNameLabel "Cont de email">
<!ENTITY accountsRestrictionsABNameLabel "Agendă">
<!ENTITY accountsRestrictionsCatNameLabel "Categorie">
<!ENTITY accountsRestrictionsIncludeNameLabel "Tip">
<!ENTITY addRestrictionLabel "Adaugă">
<!ENTITY addRestrictionAccesskey "A">
<!ENTITY renameRestrictionLabel "Editează">
<!ENTITY renameRestrictionAccesskey "E">
<!ENTITY deleteRestrictionLabel "Şterge">
<!ENTITY deleteRestrictionAccesskey "g">
<!ENTITY emailsCollectionLabel "Colectare adrese de email la expediere">
<!ENTITY emailsCollectionTooltip "Extinde colectarea Thunderbird standard a adreselor de email. Dacă această opțiune este activată, adresele de email care nu sunt prezente în agenda CardBook vor fi adăugate în agendele selectate.">
<!ENTITY emailsCollectionEnabledLabel "Activat">
<!ENTITY emailsCollectionMailNameLabel "Cont de email">
<!ENTITY emailsCollectionABNameLabel "Agendă">
<!ENTITY emailsCollectionCatNameLabel "Categorie">
<!ENTITY addEmailsCollectionLabel "Adaugă">
<!ENTITY addEmailsCollectionAccesskey "A">
<!ENTITY renameEmailsCollectionLabel "Editează">
<!ENTITY renameEmailsCollectionAccesskey "E">
<!ENTITY deleteEmailsCollectionLabel "Şterge">
<!ENTITY deleteEmailsCollectionAccesskey "g">
<!ENTITY logGroupboxLabel "Registru evenimente">
<!ENTITY debugModeLabel "Mod de depanare">
<!ENTITY debugModeAccesskey "d">
<!ENTITY debugModeTooltip "Activează modul de depanare pentru înregistrarea extinsă a evenimentelor.">
<!ENTITY statusInformationLineNumberLabel "Numărul de linii păstrate în fișierul registru">
<!ENTITY statusInformationLineNumberAccesskey "p">

<!ENTITY tabsEditingGroupboxLabel "File afișate">
<!ENTITY noteTabLabel "Notițe">
<!ENTITY noteTabAccesskey "N">
<!ENTITY listTabLabel "Listă">
<!ENTITY listTabAccesskey "L">
<!ENTITY mailPopularityTabLabel "Popularitate email">
<!ENTITY mailPopularityTabAccesskey "P">
<!ENTITY technicalTabLabel "Tehnic">
<!ENTITY technicalTabAccesskey "T">
<!ENTITY vCardTabLabel "vCard">
<!ENTITY vCardTabAccesskey "v">
<!ENTITY advancedTabLabel "Avansat">
<!ENTITY advancedTabAccesskey "A">
<!ENTITY localizeEditingGroupboxLabel "Afișează pe harta cu:">
<!ENTITY localizeEngineOpenStreetMapLabel "OpenStreetMap">
<!ENTITY localizeEngineOpenStreetMapAccesskey "O">
<!ENTITY localizeEngineGoogleMapsLabel "Hărți Google">
<!ENTITY localizeEngineGoogleMapsAccesskey "G">
<!ENTITY localizeEngineBingMapsLabel "Hărți Bing">
<!ENTITY localizeEngineBingMapsAccesskey "B">
<!ENTITY localizeTargetLabel "Deschide legăturile externe:">
<!ENTITY localizeTargetInLabel "În interiorul Thunderbird-lui">
<!ENTITY localizeTargetInAccesskey "i">
<!ENTITY localizeTargetOutLabel "În exteriorul Thunderbird-lui">
<!ENTITY localizeTargetOutAccesskey "U">
<!ENTITY showNameAsLabel "Afișează numele ca">
<!ENTITY showNameAsLFLabel "Nume de familie Prenume">
<!ENTITY showNameAsLFAccesskey "f">
<!ENTITY showNameAsLFCommaLabel "Nume de familie, Prenume">
<!ENTITY showNameAsLFCommaAccesskey "a">
<!ENTITY showNameAsFLLabel "Prenume Nume de familie">
<!ENTITY showNameAsFLAccesskey "P">
<!ENTITY showNameAsDSPLabel "Numele afișat">
<!ENTITY showNameAsDSPAccesskey "N">
<!ENTITY adrLabelExplanation "Este posibil să personalizați modul în care CardBook formatează adresele sale. Această formulă constă în blocuri închise de „()”. În aceste blocuri, există două părți delimitate de „|”. Prima parte se aplică atunci când termenul implicat, de exemplu {{1}}, nu este nul, a doua parte se aplică atunci când termenul implicat este nul.">
<!ENTITY adrFormulaLabel "Formula pentru formatul adresei">
<!ENTITY adrFormulaAccesskey "r">
<!ENTITY resetAdrFormulaLabel "Resetează">
<!ENTITY resetAdrFormulaAccesskey "z">
<!ENTITY miscEditingGroupboxLabel "Diverse">
<!ENTITY preferEmailEditionLabel "Dublu click pe un contact îl editează în loc îi timită email">
<!ENTITY preferEmailEditionAccesskey "k">
<!ENTITY dateDisplayedFormatLabel "Formatul datei:">
<!ENTITY dateDisplayedFormatAccesskey "F">
<!ENTITY preferenceGroupboxLabel "Preferința câmpului">
<!ENTITY usePreferenceValueLabel "Utilizați o valoare de preferință (numai pentru vCard 4.0)">
<!ENTITY usePreferenceValueAccesskey "p">
<!ENTITY preferenceValueLabel "Eticheta câmpului valoare de preferință">
<!ENTITY preferenceValueAccesskey "v">

<!ENTITY ABtypesGroupboxLabel "Agendă">
<!ENTITY typesCategoryGoogleLabel "Google">
<!ENTITY typesCategoryGoogleAccesskey "G">
<!ENTITY typesCategoryAppleLabel "Apple">
<!ENTITY typesCategoryAppleAccesskey "A">
<!ENTITY typesCategoryYahooLabel "Yahoo!">
<!ENTITY typesCategoryYahooAccesskey "Y">
<!ENTITY typesCategoryOthersLabel "Alte">
<!ENTITY typesCategoryOthersAccesskey "l">
<!ENTITY typesGroupboxLabel "Tipuri">
<!ENTITY typesCategoryAdrLabel "Adresă">
<!ENTITY typesCategoryAdrAccesskey "d">
<!ENTITY typesCategoryEmailLabel "Email">
<!ENTITY typesCategoryEmailAccesskey "E">
<!ENTITY typesCategoryImppLabel "Mesaj instant">
<!ENTITY typesCategoryImppAccesskey "M">
<!ENTITY typesCategoryTelLabel "Telefon">
<!ENTITY typesCategoryTelAccesskey "T">
<!ENTITY typesCategoryUrlLabel "Adresa URL">
<!ENTITY typesCategoryUrlAccesskey "U">
<!ENTITY typesLabelLabel "Eticheta">
<!ENTITY addTypeLabel "Adaugă">
<!ENTITY addTypeAccesskey "A">
<!ENTITY renameTypeLabel "Editează">
<!ENTITY renameTypeAccesskey "E">
<!ENTITY deleteTypeLabel "Şterge">
<!ENTITY deleteTypeAccesskey "g">
<!ENTITY resetTypeLabel "Resetează">
<!ENTITY resetTypeAccesskey "z">

<!ENTITY IMPPGroupboxWarn "Această configurare permite comunicații pe Internet utilizând protocolul dorit.">
<!ENTITY IMPPGroupboxLabel "Comunicații pe Internet">
<!ENTITY preferIMPPPrefLabel "Preferă comunicațiile pe Internet marcate cu ">
<!ENTITY preferIMPPPrefAccesskey "P">
<!ENTITY preferIMPPPrefWarn "Dacă nu este marcată nici o adresă, se propune o conexiune pentru toate adresele contactului.">
<!ENTITY IMPPCodeLabel "Cod">
<!ENTITY IMPPLabelLabel "Etichetă">
<!ENTITY IMPPProtocolLabel "Protocol">
<!ENTITY addIMPPLabel "Adaugă">
<!ENTITY addIMPPAccesskey "A">
<!ENTITY renameIMPPLabel "Editează">
<!ENTITY renameIMPPAccesskey "E">
<!ENTITY deleteIMPPLabel "Şterge">
<!ENTITY deleteIMPPAccesskey "g">

<!ENTITY customFieldRankLabel "Rang">
<!ENTITY customFieldCodeLabel "Nume câmp">
<!ENTITY customFieldLabelLabel "Etichetă câmp">
<!ENTITY customFieldsPersTitleLabel "Informații personale">
<!ENTITY customFieldsPersTitleAccesskey "p">
<!ENTITY customFieldsOrgTitleLabel "Informații profesionale">
<!ENTITY customFieldsOrgTitleAccesskey "r">
<!ENTITY addCustomFieldsLabel "Adaugă">
<!ENTITY addCustomFieldsAccesskey "A">
<!ENTITY renameCustomFieldsLabel "Editează">
<!ENTITY renameCustomFieldsAccesskey "E">
<!ENTITY deleteCustomFieldsLabel "Şterge">
<!ENTITY deleteCustomFieldsAccesskey "g">

<!ENTITY orgWarn "Câmpul ORG poate conține o valoare de text structurată care constă din componente separate prin caracterul PUNCT ŞI VIRGULĂ (de exemplu: companiaDvs;serviciulDvs;departamentulDvs). Câmpurile de mai jos descriu etichetele componente ale acestei structuri.">

<!ENTITY listPanelLabel "Dacă serverul tău nu acceptă categorii (cum ar fi Google, Apple, Zimbra, etc…), listele sunt o modalitate bună de a trimite email-uri către grupuri. VCard 3.0 nu suportă liste în mod nativ, dar acest lucru poate fi realizat cu ajutorul câmpurilor personalizate, deci dacă dorești să utilizezi liste în vCard 3.0, trebuie să definești aceste câmpuri.">
<!ENTITY listGroupboxLabel "Câmpuri personalizate pentru liste">
<!ENTITY kindCustomLabel "Tip">
<!ENTITY kindCustomAccesskey "T">
<!ENTITY memberCustomLabel "Membru">
<!ENTITY memberCustomAccesskey "M">
<!ENTITY resetListLabel "Resetează">
<!ENTITY resetListAccesskey "z">

<!ENTITY syncGroupboxLabel "Sincronizare">
<!ENTITY syncWarn "Sincronizarea propagă modificările tale către un server la distanță și modificările de pe server către tine.">
<!ENTITY initialSyncLabel "Rulează sincronizarea la pornirea Thunderbird">
<!ENTITY initialSyncAccesskey "R">
<!ENTITY initialSyncDelayLabel "Întârzierea inițială a sincronizării (secunde)">
<!ENTITY initialSyncDelayAccesskey "z">
<!ENTITY solveConflictsLabel "Dacă un contact a fost actualizat atât pe server, cât și local:">
<!ENTITY solveConflictsLocalLabel "Preferă modificarea locală">
<!ENTITY solveConflictsLocalAccesskey "l">
<!ENTITY solveConflictsRemoteLabel "Preferă modificarea de pe server">
<!ENTITY solveConflictsRemoteAccesskey "s">
<!ENTITY solveConflictsUserLabel "Întrebă utilizatorul">
<!ENTITY solveConflictsUserAccesskey "a">
<!ENTITY maxModifsPushedLabel "Numărul maxim de modificări transmise pe sincronizare">
<!ENTITY maxModifsPushedAccesskey "m">
<!ENTITY requestsGroupboxLabel "Cereri">
<!ENTITY requestsTimeoutLabel "Timp de așteptare (secunde)">
<!ENTITY requestsTimeoutAccesskey "T">
<!ENTITY multigetLabel "Pentru solicitările GET, cere contactele în grupuri de ">
<!ENTITY multigetAccesskey "G">
<!ENTITY discoveryGroupboxLabel "Descoperirea agendelor">
<!ENTITY discoveryWarn "La fiecare sincronizare, se vor verifica adresele URL de mai jos pentru a căuta agende noi sau șterse">
<!ENTITY discoveryAccountsGroupboxLabel "Conturi">
<!ENTITY advancedSyncGroupboxLabel "Proprietăți avansate">
<!ENTITY decodeReportLabel "Decodează adresele URL pentru solicitările REPORT">
<!ENTITY decodeReportAccesskey "D">

<!ENTITY miscEmailGroupboxLabel "Trimitere email-uri">
<!ENTITY preferEmailPrefLabel "Preferă adresele de email marcate cu ">
<!ENTITY preferEmailPrefAccesskey "P">
<!ENTITY preferEmailPrefWarn "Dacă nu este marcată nici o adresă, mesajul este trimis la toate adresele contactului.">
<!ENTITY warnEmptyEmailsLabel "Avertizează-mă când încerc să trimit email către un contact fără adresă de email">
<!ENTITY warnEmptyEmailsAccesskey "A">
<!ENTITY useOnlyEmailLabel "Utilizează numai adresele de email, fără numele lor de afișare">
<!ENTITY useOnlyEmailAccesskey "U">
<!ENTITY attachVCardWarn "Pentru a adăuga automat un vCard la email-urile tale, selectează cate un vCard pentru fiecare cont de email. Dacă înregistrarea este activată, CardBook va atașa acest vCard fiecărui email trimis din acest cont.">
<!ENTITY attachVCardGroupboxLabel "Adăugare vCard la email-uri">
<!ENTITY accountsVCardsEnabledLabel "Activat">
<!ENTITY accountsVCardsMailLabel "Cont de email">
<!ENTITY accountsVCardsABNameLabel "Agendă">
<!ENTITY accountsVCardsFileNameLabel "Fișier atasat">
<!ENTITY accountsVCardsFnLabel "Contact">
<!ENTITY addVCardLabel "Adaugă">
<!ENTITY addVCardAccesskey "A">
<!ENTITY renameVCardLabel "Editează">
<!ENTITY renameVCardAccesskey "E">
<!ENTITY deleteVCardLabel "Şterge">
<!ENTITY deleteVCardAccesskey "g">

<!ENTITY birthdayListWarn "Aniversările sunt căutate doar în agendele CardBook. Formatele pentru datele aniversare sunt configurate în proprietățile agendei.">
<!ENTITY addressBooksGroupboxLabel "Agende">
<!ENTITY calendarsGroupboxLabel "Calendare">
<!ENTITY searchGroupboxLabel "Căutare">
<!ENTITY searchInNoteLabel "Căută aniversările evenimentului în câmpul de Notițe">
<!ENTITY searchInNoteTooltip1 "Adăugă astfel de linii în câmpul Notițe (separatorul ar trebui să fie : și formatul datei ar trebui să fie cel definit pentru agengă) :">
<!ENTITY searchInNoteTooltip2 "Eveniment:Descriere:2006-12-24">
<!ENTITY searchInNoteAccesskey "C">

<!ENTITY remindersTabLabel "Mementouri">
<!ENTITY remindViaPopupWindowGroupboxLabel "Reamintire prin fereastră pop-up">
<!ENTITY numberOfDaysForSearchingLabel "Numărul de zile pentru verificarea aniversărilor">
<!ENTITY numberOfDaysForSearchingAccesskey "N">
<!ENTITY showPopupOnStartupLabel "Afișează pop-up la pornire">
<!ENTITY showPopupOnStartupAccesskey "A">
<!ENTITY showPeriodicPopupLabel "Afișează pop-up zilnic">
<!ENTITY showPeriodicPopupAccesskey "z">
<!ENTITY periodicPopupTimeLabel "Afișează la ora (oo:mm)">
<!ENTITY periodicPopupTimeAccesskey "i">
<!ENTITY showPopupEvenIfNoBirthdayLabel "Afișează pop-up atunci când lista aniversărilor este goală">
<!ENTITY showPopupEvenIfNoBirthdayAccesskey "g">
<!ENTITY remindViaLightningGroupboxLabel "Reamintire prin extensia Thunderbird Lightning Calendar">
<!ENTITY numberOfDaysForWritingLabel "Numărul de zile pentru a scrie aniversări">
<!ENTITY numberOfDaysForWritingAccesskey "s">
<!ENTITY syncWithLightningOnStartupLabel "Sincronizează cu Lightning la pornire">
<!ENTITY syncWithLightningOnStartupAccesskey "L">
<!ENTITY calendarEntryTitleLabel "Titlul pentru evenimentul din calendar">
<!ENTITY calendarEntryTitleTooltip "Titlul șablonului pentru evenimentul din calendar. Ar trebui să conțină doi substituenți &#37;S, respectiv înlocuiți de nume și vârstă. Dacă acest câmp este gol, se utilizează un mesaj standard.">
<!ENTITY calendarEntryTitleAccesskey "T">
<!ENTITY resetCalendarEntryTitleButtonLabel "Resetează">
<!ENTITY resetCalendarEntryTitleButtonAccesskey "z">
<!ENTITY calendarEntryTimeLabel "Ora pentru evenimentul din calendar (oo:mm)">
<!ENTITY calendarEntryTimeAccesskey "O">
<!ENTITY calendarEntryWholeDayLabel "Eveniment calendaristic pentru întreaga zi">
<!ENTITY calendarEntryWholeDayAccesskey "r">
<!ENTITY calendarEntryAlarmLabel "Alarmă la n ore înaintea evenimentul din calendar (Exemplu: 4,+1)">
<!ENTITY calendarEntryAlarmTooltip "Introdu cu câte ore înainte de aniversare trebuie sa îți reamintească Lightning. Dacă acest câmp este gol, nu va exista nici o alarmă. Dacă vrei mai multe alarme, introdu alarmele într-o listă separată prin virgulă.">
<!ENTITY calendarEntryAlarmAccesskey "n">
<!ENTITY calendarEntryCategoriesLabel "Categorii pentru evenimentul din calendar">
<!ENTITY calendarEntryCategoriesTooltip "Una sau mai multe categorii separate prin virgulă. Dacă acest câmp este gol, nu va exista nici o categorie.">
<!ENTITY calendarEntryCategoriesAccesskey "g">
